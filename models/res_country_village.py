# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ResCountryVillage(models.Model):
    _name = "res.country.village"

    name = fields.Char("Name")
    city_id = fields.Many2one('res.country.city', "City")

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

# -*- coding: utf-8 -*-


from odoo import models, fields, api


class ResCountryCity(models.Model):
    _name = 'res.country.city'

    name = fields.Char("Name")
    pincode = fields.Char("Zip")
    state_id = fields.Many2one("res.country.state", "State")

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

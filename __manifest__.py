# -*- coding: utf-8 -*-

{
    'name': 'GKTS Community',
    'version': '1.1',
    'summery': 'Make Pepole together',
    'sequence': 1,
    'description': """

    """,
    'category': 'Social',
    'website': 'https://www.gkts.com',
    'images': [],
    'depends': [
        'base',
        'contacts',
        'website',
        'im_livechat',
    ],
    'data': [
        'view/res_country_city_view.xml',
        'view/res_country_village_view.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
